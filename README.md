# Deploy microservices to AWS ECS with CloudFormation

This repo is based on the official awslabs/ecs-refarch-cloudformation repository providing a well-architected environment for the demo application [henrybravo/refarch-demos-nodejsapp-service](https://gitlab.com/henrybravo/someone-yells)

This template deploys a VPC, with a pair of public and private subnets spread across two Availabilty Zones. It deploys an Internet Gateway, with a default route on the public subnets. It deploys a pair of NAT Gateways (one in each AZ), and default routes for them in the private subnets.

It then deploys a highly available ECS cluster using an AutoScaling Group, with ECS hosts distributed across multiple Availability Zones. 

Finally, it deploys a demo ECS service from containers published in Amazon EC2 Container Registry (Amazon ECR) build from the demo application from [henrybravo/refarch-demos-nodejsapp-service](https://gitlab.com/henrybravo/someone-yells)


